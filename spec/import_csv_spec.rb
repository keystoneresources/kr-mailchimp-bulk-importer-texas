require_relative "../import_csv"

describe ImportCsv do
  it "should import subscribers"
  it "should have the same ammount of records as the file has rows"
  it "should have the correct values from Subscriber"
  it "should save values to the database"
  it "should accept a parameter to define what folder to get csv from"
  it "should log any save errors"

end

describe MailChimp do

  it "should upload subscribers"
  it "should save uploaded subscribers"
  it "should not upload subscribers that have already been uploaded"
  it "should save bad subscribers"
  it "should store the MailChimp code and message for BadSubscribers"
  it "should let us know who was uploaded"
  it "should let us know who wasn't uploaded"
  it "should save a csv of BadSubscribers and there error codes"
  it "should save a csv of who was imported"

end

describe Subscriber do

  before {subject.stub(
    :email => "roger+1@keystoneresources.com",
    :fname => "Keenan",
    :lname => "Pawley",
    :date_added => "2013-03-04 22:14:55",
    :street => "72 Old Farm Rd",
    :street2 => "",
    :city => "Bedford",
    :state => "NH",
    :province => "",
    :zip => "",
    :zip2 => "03110-5734",
    :country => "US",
    :address => "",
    :company => "",
    :phone => "",
    :gender => "M",
    :sent_to_mailchimp => false,
    :mailchimp_response => "",
    :timestamp => "",
    :mailchimped_at => ""
  )}

  let(:subscriber_row) {{
    "email" => "roger+1@keystoneresources.com",
    "fname" => "Keenan",
    "lname" => "Pawley",
    "date_added" => "2013-03-04 22:14:55",
    "street" => "72 Old Farm Rd",
    "street2" => "",
    "city" => "Bedford",
    "state" => "NH",
    "province" => "",
    "zip" => "",
    "zip2" => "03110-5734",
    "country" => "US",
    "address" => "",
    "company" => "",
    "phone" => "",
    "gender" => "F"
  }}

  it "should have an email address" do
    subject.email.should eq("roger+1@keystoneresources.com")
  end
  it "should have a first name" do
    subject.fname.should eq("Keenan")
  end
  it "should have a last name" do
    subject.lname.should eq("Pawley")
  end

  it "should have all the correct fields"
  it "should have all the correct values in the fields for the first test record"

  # it { should have_field(
#               :email,
#               :fname,
#               :lname,
#               :date_added,
#               :street,
#               :street2,
#               :city,
#               :state,
#               :province,
#               :zip,
#               :zip2,
#               :country,
#               :address,
#               :company,
#               :phone,
#               :gender,
#               :sent_to_mailchimp,
#               :mailchimp_response,
#               :timestamp,
#               :mailchimped_at
#       )}
#   it { should validate_presence_of(:name) }
#
#   let(:subscriber_row) {{
#     "Email Address" => "roger+1@keystoneresources.com",
#     "First Name" => "Keenan",
#     "Last Name" => "Pawley",
#     "date added" => "2013-03-04 22:14:55",
#     "Street" => "72 Old Farm Rd",
#     "Street Cont." => "",
#     "City" => "Bedford",
#     "State" => "NH",
#     "Province" => "",
#     "Zip / Postal Code" => "",
#     "Zip / Postal Code Cont." => "03110-5734",
#     "Country" => "US",
#     "Full Address" => "",
#     "Company" => "",
#     "Phone Number" => "",
#     "Gender" => "M"
#   }}
#
#
#   it "fetches the email and stores it" do
#     Subscriber.new(subscriber).email.should eq("roger+1@keystoneresources.com")
#   end
#   it "fetches the first name and stores it" do
#     Subscriber.new(subscriber).fname.should eq("Keenan")
#   end
#   it "fetches the last name and stores it" do
#     Subscriber.new(subscriber).lname.should eq("Pawley")
#   end
#   it "fetches the date added and stores it" do
#     Subscriber.new(subscriber).date_added.should eq("2013-03-04 22:14:55")
#   end
#   it "fetches the street and stores it" do
#     Subscriber.new(subscriber).street.should eq("72 Old Farm Rd")
#   end
#   it "fetches the street2 and stores it" do
#     Subscriber.new(subscriber).street2.should eq("")
#   end
#   it "fetches the city and stores it" do
#     Subscriber.new(subscriber).city.should eq("Bedford")
#   end
#   it "fetches the state and stores it" do
#     Subscriber.new(subscriber).state.should eq("NH")
#   end
#   it "fetches the province and stores it" do
#     Subscriber.new(subscriber).province.should eq("")
#   end
#   it "fetches the zip and stores it" do
#     Subscriber.new(subscriber).zip.should eq("")
#   end
#   it "fetches the zip2 and stores it" do
#     Subscriber.new(subscriber).zip2.should eq("03110-5734")
#   end
#   it "fetches the country and stores it" do
#     Subscriber.new(subscriber).country.should eq("US")
#   end
#   it "fetches the address and stores it" do
#     Subscriber.new(subscriber).address.should eq("")
#   end
#   it "fetches the company and stores it" do
#     Subscriber.new(subscriber).company.should eq("")
#   end
#   it "fetches the phone number and stores it" do
#     Subscriber.new(subscriber).phone.should eq("")
#   end
#   it "fetches the gender and stores it" do
#     Subscriber.new(subscriber).gender.should eq("M")
#   end
end