class CreateBadSubscribers < ActiveRecord::Migration

  def change
    create_table :bad_subscribers do |t|
      t.string :email, :unique => true
      t.string :fname
      t.string :lname
      t.string :date_added
      t.string :street
      t.string :street2
      t.string :city
      t.string :state
      t.string :province
      t.string :zip
      t.string :zip2
      t.string :country
      t.string :address
      t.string :company
      t.string :phone
      t.string :gender
      t.boolean :sent_to_mailchimp, :default => false
      t.datetime :mailchimped_at
      t.text :mailchimp_code
      t.text :mailchimp_message
      t.text :collection_method
      t.timestamps :timestamp
    end
  end
end