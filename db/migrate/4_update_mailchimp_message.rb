class UpdateMailchimpMessage < ActiveRecord::Migration

  def change
    change_table :subscribers do |t|
      t.remove :mailchimp_message
      t.text :mailchimp_message
    end
  end
end