class UpdateSubscriberMailchimp < ActiveRecord::Migration

  def change
    change_table :subscribers do |t|
      t.remove :mailchimp_response
      t.string :mailchimp_code
      t.string :mailchimp_message
      t.string :collection_method
    end
  end
end