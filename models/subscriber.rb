class Subscriber < ActiveRecord::Base

  def custom_merge_vars
    {
      :EMAIL => email,
      :FNAME => fname,
      :LNAME => lname,
      :MMERGE3 => date_added,
      :MMERGE10 => street,
      :MMERGE14 => street2,
      :MMERGE11 => city,
      :MMERGE8 => state,
      :MMERGE13 => province,
      :MMERGE12 => zip,
      :MMERGE15 => zip2,
      :MMERGE9 => country,
      :MMERGE5 => address,
      :MMERGE6 => company,
      :MMERGE7 => phone,
      :MMERGE4 => gender
    }
  end
end

class BadSubscriber < ActiveRecord::Base

end