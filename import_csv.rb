require 'rubygems'
require 'bundler/setup'
require 'csv'
require 'gibbon'
require 'logger'
require 'fileutils'

require_relative "db/setup"
Dir.glob("./models/**.rb").each {|f| require f}


class ImportCsv

  def import_subscribers
    logger = Logger.new("logs/processing.log")

    path = "csv/sweeps/import.csv"
    i = 0
    t = Time.now

    CSV.foreach(path,:headers=>true) do |row|
      values = row.to_hash
      values.delete(nil) # the CSV has a nil column
      email = values.fetch("email")
      subscriber = Subscriber.where(:email => email).first_or_create
      subscriber.update_attributes(values)
      subscriber.update_attributes(:collection_method => 'sweeps')
      if !subscriber.save
        logger.error {'[ERROR] - #{row.email} : #{row.errors.full_messages.join(", ")'}
      end
      i+=1
      puts i
    end

    FileUtils.mv("csv/sweeps/import.csv", "csv/imported/imported_#{t.year}-#{t.month}-#{t.day}_#{t.hour}-#{t.min}.csv")
  end
end


class ExportCSV

  def not_sent
    t = Time.now
    File.open("csv/data_exports/not_sent_to_mailchimp_#{t.year}-#{t.month}-#{t.day}_#{t.hour}.csv", "wb") do |file|
      Subscriber.where(:sent_to_mailchimp=>false,:collection_method=>"resubs").pluck(:email).each do |email|
        file << email << "\n"
      end
    end
  end
end


class MailChimp

  def upload_subscribers
    logger = Logger.new("logs/uploading.log")

    gb = Gibbon.new("61618c0fc37809279a82910b4d63cc8d-us4")
    imported_subscribers = []
    bad_subscribers = []
    i = 0

    Subscriber.where(:sent_to_mailchimp => false, :collection_method => "sweeps").each do |subscriber|
    #
      begin
        gb.list_subscribe({
          :id => "263a80ec58",
          :email_address => subscriber.email,
          :merge_vars => subscriber.custom_merge_vars()
        })
        Subscriber.update(subscriber.id,
          :sent_to_mailchimp => true,
          :mailchimped_at => Time.now.getutc,
          :mailchimp_code => nil,
          :mailchimp_message => nil)
        imported_subscribers << {
          :id => subscriber.id,
          :email => subscriber.email,
          :mailchimped_at => Time.now.getutc}

      rescue Gibbon::MailChimpError => mce
        Subscriber.update(subscriber.id,
          :sent_to_mailchimp => true,
          :mailchimp_code => mce.code,
          :mailchimp_message => mce.message)
        bad_subscriber = BadSubscriber.where(:email => subscriber.email).first_or_create
        bad_subscriber.update_attributes({
          :email => subscriber.email,
          :mailchimp_code => mce.code,
          :mailchimp_message => mce.message,
          :mailchimped_at => subscriber.mailchimped_at
        })
        if !bad_subscriber.save
          logger.error {'[ERROR] - #{subscriber.email} : #{subscriber.errors.full_messages.join(", ")'}
        end

        bad_subscribers << {
          :id => subscriber.id,
          :email => subscriber.email,
          :mailchimp_code => mce.code,
          :mailchimp_message => mce.message}

      next
      end
      i+=1
      puts i
    end


    puts "Imported #{imported_subscribers.count} subscribers."
    puts "Did Not Import #{bad_subscribers.count} subscribers."
    if bad_subscribers.count > 0
      puts bad_subscribers
      logger.error { bad_subscribers }
    end
  end
end